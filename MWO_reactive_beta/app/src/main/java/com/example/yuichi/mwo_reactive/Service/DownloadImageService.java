package com.example.yuichi.mwo_reactive.Service;

import android.graphics.drawable.Drawable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import rx.Observable;

/**
 * Created by Yuichi on 2015-11-02.
 */
public class DownloadImageService {
    public static Observable<Drawable> getImageFromUrl(String url) { //funkcja służy do pobierania z urla, WAŻNE - zwraca obiekt Observable z pakietu rx
        InputStream inputStream = null;                              // pakiet rx jest dociągany do projektu przy buildzie, dzięki temu że jest on dodany
        try {                                                        // do compile w dependencies
            inputStream = (InputStream) new URL(url).getContent();   // W tej linii pobierana jest zawartość z url-a jako ciąg bajtów
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        Drawable drawable = Drawable.createFromStream(inputStream, "image"); //tutaj z ciągu bajtów składa Drawable
        return drawable != null ? Observable.just(drawable) : null;          // uproszczona składnia if - jak drawable nie jest null to go zwraca
    }
}

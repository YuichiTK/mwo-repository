package com.example.yuichi.mwo_reactive.Activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.yuichi.mwo_reactive.R;
import com.example.yuichi.mwo_reactive.Service.DownloadImageService;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import rx.Observer;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        final EditText editText = (EditText)findViewById(R.id.editText);
        Button button = (Button)findViewById(R.id.button);
        final ImageView imageView = (ImageView)findViewById(R.id.imageView);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText.getText() != null) //jeżeli url nie jest null
                DownloadImageService.getImageFromUrl(editText.getText().toString()) //wywolujemy funkcje która zwraca obiekt typu Observable (pakiet rx)
                        .observeOn(AndroidSchedulers.mainThread()) // każemy mu nasłuchiwać na głównym wątku na wykonanie powyższej metody
                        .subscribe(new Observer<Drawable>() { //tutaj dzieje się wszystko po tym jak metoda zwróci wynik, jak by sie komuś chciało
                            @Override                           // to moze dodac do projektu retrolambde i zamienić całe 10 linijek na jedną
                            public void onCompleted() {         // .subscribe(image -> { imageView.setDrawable(drawable); } (<- z retrolambdą)
                            }                                   // wgl to można dodać picasso i ładowac picassem
                                                                // ok poza tym databinding i to by trzeba było zrobić
                            @Override                           // zabezpieczyć przed błędami - jak jest zły url to sie sypie bo próbuje obserwować null-a
                            public void onError(Throwable e) {  // no i to tyle :P usuncie moje komentarze i dodajcie jakies sensowne jakby to babka sprawdzała kiedyś

                            }

                            @Override
                            public void onNext(Drawable drawable) {
                                imageView.setImageDrawable(drawable);
                            }
                        });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
